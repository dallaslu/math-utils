package com.dallaslu.math.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AryConverter {

	public static Integer[] convertAry(Integer[] src, int srcA, int distA) {
		List<Integer> srcList = new ArrayList<Integer>(Arrays.asList(src));
		List<Integer> distList = new ArrayList<Integer>();

		List<Integer> quotient = srcList;
		while (quotient.size() > 0) {
			int remainder = 0;
			List<Integer> tempQuotient = quotient;
			quotient = new ArrayList<Integer>();
			while (tempQuotient.size() > 0) {
				Integer foo = tempQuotient.get(0);
				tempQuotient.remove(0);

				int val = foo.intValue();

				int dividend = remainder * srcA + val;

				int result = dividend / distA;
				remainder = dividend % distA;

				quotient.add(result);
			}

			while (quotient.size() > 0 && quotient.get(0) == 0) {
				quotient.remove(0);
			}

			distList.add(0, remainder);
		}

		return distList.toArray(new Integer[0]);
	}
}
